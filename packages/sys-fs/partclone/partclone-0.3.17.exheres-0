# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Distributed under the terms of the GNU General Public License v2

require github [ user="Thomas-Tsai" tag=${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="provides utilities to backup a partition smartly and it is designed
for higher compatibility of the file system by using existing library."

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    fuse [[ description = [ enable imgfuse, mount image file to block files ] ]]
    btrfs [[ description = [ enable btrfs file system ] ]]
    exfat [[ description = [ enable EXFAT file system ] ]]
    extfs [[ description = [ enable ext2/3/4 file system ] ]]
    fat [[ description = [ enable FAT file system ] ]]
    f2fs [[ description = [ enable f2fs file system ] ]]
    hfsp [[ description = [ enable HFS plus file system ] ]]
    jfs [[ description = [ enable jfs file system ] ]]
    minix [[ description = [ enable minix file system ] ]]
    ntfs [[ description = [ enable NTFS file system ] ]]
    xfs [[ description = [ enable XFS file system ] ]]
"

DEPENDENCIES="
    build+run:
        sys-libs/ncurses
        btrfs? ( sys-fs/btrfs-progs )
        extfs? ( sys-fs/e2fsprogs )
        fuse? ( sys-fs/fuse:3 )
        jfs? ( sys-fs/jfsutils )
        ntfs? ( sys-fs/ntfs-3g_ntfsprogs )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-mtrace
    --disable-nilfs2 #nilfs-utils is in graveyard
    --disable-reiser4 #reiser4progs is in graveyard
    --disable-reiserfs #reiserfsprogs doesn't currently compile
    --disable-static
    --disable-ufs
    --disable-vmfs
    --enable-ncursesw
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    btrfs
    exfat
    extfs
    fat
    f2fs
    hfsp
    jfs
    minix
    ntfs
    xfs
)

DEFAULT_SRC_CONFIGURE_TESTS=( "--enable-fs-test --disable-fs-test" )

#tests all fail for some reason
RESTRICT="test"

src_prepare(){
    edo sed -i -r 's/pkg-config/'$(exhost --target)'-pkg-config/g' configure.ac
    autotools_src_prepare
}

