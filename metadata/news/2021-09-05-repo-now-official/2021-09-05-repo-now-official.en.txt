Title: ::tombriden is now an official dev repo
Author: Tom Briden <tom@decompile.me.uk>
Content-Type: text/plain
Posted: 2021-09-05
Revision: 1
News-Item-Format: 1.0

The ::tombriden repository has been moved from Gitlab to git.exherbo.org
because it is an official dev repo now. The old repository on Gitlab won't be
updated anymore, please update your configs accordingly by changing the sync line
in /etc/paludis/repositories/tombriden.conf to the following:

    sync = git+https://git.exherbo.org/git/dev/tombriden.git
